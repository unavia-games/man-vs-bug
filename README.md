# Man vs Bug

[<img src="https://img.shields.io/badge/unity-v2019.3-blue?logo=unity" />](https://unity.com)

Unity game developed over the frustrations of the never ending cycle between bugs and devs.

