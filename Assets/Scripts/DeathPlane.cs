﻿using UnityEngine;

public class DeathPlane : MonoBehaviour
{
    #region Unity Methods
    void OnCollisionEnter(Collision collision)
    {
        // Colliding with the death plane immediately kills anything that can take damage
        Damageable damageable = collision.gameObject.GetComponent<Damageable>();
        if (damageable == null) return;

        damageable.Kill(gameObject);
    }
    #endregion
}

