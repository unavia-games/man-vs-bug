﻿using Drawing;
using UnityEngine;

[RequireComponent(typeof(Player))]
[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    #region Fields
    #endregion

    private Player player;
    private Rigidbody rb;
    private Vector3 velocity;
    private Vector3 lookPoint;


    #region Unity Methods
    private void Awake()
    {
        player = GetComponent<Player>();
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        Vector3 correctedLookPoint = new Vector3(lookPoint.x, transform.position.y, lookPoint.z);
        transform.LookAt(correctedLookPoint);

        // DEBUG
        Draw.ingame.ArrowheadArc(transform.position, transform.forward, 0.5f, Color.blue);
    }

    private void FixedUpdate()
    {
        rb.MovePosition(transform.position + velocity * Time.fixedDeltaTime);
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Move the player in a direction
    /// </summary>
    /// <param name="direction">Movement direction (normalized)</param>
    public void Move(Vector3 direction)
    {
        velocity = direction * player.Speed;
    }

    /// <summary>
    /// Look at a point
    /// </summary>
    /// <param name="lookPoint">Look point</param>
    public void LookAt(Vector3 lookPoint)
    {
        this.lookPoint = lookPoint;
    }
    #endregion
}
