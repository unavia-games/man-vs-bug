﻿using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(PlayerController))]
[RequireComponent(typeof(WeaponController))]
public class PlayerInput : MonoBehaviour
{
	#region Fields
	#endregion

    private PlayerActionMap playerActions;
    private PlayerController playerController;
    private WeaponController weaponController;


    #region Unity Methods
    void Awake()
    {
        playerActions = new PlayerActionMap();
    }

    private void Start()
    {
        playerController = GetComponent<PlayerController>();
        weaponController = GetComponent<WeaponController>();
    }

    private void OnEnable()
    {
        playerActions.Player.Enable();
        playerActions.Weapon.Enable();
        playerActions.Weapon.Fire.performed += _ => OnFire(true);
        playerActions.Weapon.Fire.canceled += _ => OnFire(false);
        playerActions.Weapon.Reload.performed += _ => OnReloadWeapon();
        playerActions.Weapon.SwitchWeapon.performed += _ => OnSwitchWeapon();
    }

    private void OnDisable()
    {
        playerActions.Player.Disable();
        playerActions.Weapon.Disable();
        playerActions.Weapon.Fire.performed -= _ => OnFire(true);
        playerActions.Weapon.Fire.canceled -= _ => OnFire(false);
        playerActions.Weapon.Reload.performed -= _ => OnReloadWeapon();
        playerActions.Weapon.SwitchWeapon.performed -= _ => OnSwitchWeapon();
    }

    void Update()
    {
        HandlePlayerInput();
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Handle player input
    /// </summary>
    private void HandlePlayerInput()
    {
        Vector2 moveInput = playerActions.Player.Move.ReadValue<Vector2>();
        Vector3 moveDirection = new Vector3(moveInput.x, 0f, moveInput.y);
        playerController.Move(moveDirection.normalized);

        Vector3 mouseInput = Mouse.current.position.ReadValue();
        Vector3 mousePoint = MouseUtilities.GetMouseLookPoint(mouseInput, transform.position.y);
        playerController.LookAt(mousePoint);
    }

    private void OnFire(bool pressed)
    {
        weaponController.Fire(pressed);
    }

    private void OnReloadWeapon()
    {
        weaponController.Reload();
    }

    private void OnSwitchWeapon()
    {
        weaponController.EquipNextWeapon();
    }
    #endregion
}
