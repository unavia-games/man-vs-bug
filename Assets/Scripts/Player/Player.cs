﻿using com.ootii.Messages;
using UnityEngine;

[RequireComponent(typeof(Damageable))]
[RequireComponent(typeof(PlayerInput))]
[RequireComponent(typeof(PlayerController))]
[RequireComponent(typeof(WeaponController))]
public class Player : MonoBehaviour
{
    #region Fields
    [SerializeField]
    [Range(0f, 10f)]
    private float speed = 5f;
    #endregion

    public Damageable Damageable { get; private set; }
    public bool Alive => Damageable.Alive;
    public float Speed => speed;

    #region Unity Methods
    private void Awake()
    {
        Damageable = GetComponent<Damageable>();

        // Subscribe to own damage and death events
        Damageable.OnDamage += OnDamage;
        Damageable.OnDeath += OnDeath;
    }
    #endregion


    #region Custom Methods
    private void OnDamage(float damage, GameObject damager)
    {
        MessageDispatcher.SendMessage(GameEventStrings.PLAYER__DAMAGED);
    }

    private void OnDeath(GameObject killer)
    {
        MessageDispatcher.SendMessage(GameEventStrings.PLAYER__DEATH);
    }
    #endregion
}
