﻿using Sirenix.OdinInspector;
using System.Collections.Generic;
using Unavia.Utilities;
using UnityEngine;

/// <summary>
/// Weapon controller state
/// </summary>
public enum WeaponControllerState
{
    EQUIPPING,
    READY
}

public class WeaponController : ExtendedMonoBehaviour
{
    #region Fields
    [SerializeField]
    private Transform weaponTransform = default;
    [SerializeField]
    [ReadOnly]
    private Weapon equippedWeapon;
    public List<Weapon> Weapons = new List<Weapon>();
    #endregion

    public WeaponControllerState State { get; private set; }
    private bool hasGunEquipped => equippedWeapon != null;
    private int equippedWeaponIndex;

    // Continuous firing is tracked separately
    private bool isFireKeyHeld = false;

    #region Unity Methods
    void Awake()
    {
        State = WeaponControllerState.READY;

        if (!hasGunEquipped && Weapons.Count > 0)
        {
            EquipWeapon(Weapons[0], true);
        }
    }

    void Update()
    {
        if (isFireKeyHeld && equippedWeapon != null && equippedWeapon.WeaponFireType == WeaponFireType.AUTOMATIC)
        {
            equippedWeapon.Fire();
        }
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Equip a weapon
    /// </summary>
    /// <param name="weapon">Weapon to equip</param>
    /// <param name="skipEquipTime">Whether equip time is skipped</param>
    public void EquipWeapon(Weapon weapon, bool skipEquipTime = false)
    {
        if (State == WeaponControllerState.EQUIPPING) return;
        isFireKeyHeld = false;

        // TODO: Figure out better system for keeping gun state across equips...
        if (equippedWeapon != null)
        {
            Destroy(equippedWeapon.gameObject);
        }

        // Prevent switching to weapon the player does not have
        equippedWeaponIndex = Weapons.FindIndex(x => x == weapon);
        if (equippedWeaponIndex < 0) return;

        State = WeaponControllerState.EQUIPPING;
        float equipDelay = skipEquipTime ? 0f : weapon.EquipTime;
        Wait(equipDelay, () =>
        {
            equippedWeapon = Instantiate(
                weapon,
                weaponTransform.position,
                weaponTransform.rotation,
                weaponTransform
            );

            State = WeaponControllerState.READY;
        });
    }

    /// <summary>
    /// Equip the next weapon
    /// </summary>
    public void EquipNextWeapon()
    {
        // Only switch guns if there is another gun
        if (Weapons.Count <= 1) return;

        int nextWeaponIndex = equippedWeaponIndex < Weapons.Count - 1 ? equippedWeaponIndex++ : 0;
        EquipWeapon(Weapons[nextWeaponIndex]);
    }

    /// <summary>
    /// Fire the weapon (supports continuous firing)
    /// </summary>
    /// <param name="started">Whether shot was fired (or released)</param>
    public void Fire(bool started)
    {
        if (State != WeaponControllerState.READY) return;
        isFireKeyHeld = started;

        if (!started) return;

        equippedWeapon.Fire();
    }

    /// <summary>
    /// Reload the weapon
    /// </summary>
    public void Reload()
    {
        if (State != WeaponControllerState.READY) return;
        isFireKeyHeld = false;

        equippedWeapon.Reload();
    }
    #endregion
}
