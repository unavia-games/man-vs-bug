﻿using Unavia.Utilities;
using UnityEngine;

public class Projectile : ExtendedMonoBehaviour
{
    #region Fields
    [Header("Properties")]
    [SerializeField]
    private LayerMask collisionMask = ~0;

    [Header("Effects")]
    [SerializeField]
    private GameObject hitEffect = default;
    [SerializeField]
    private AudioClip hitSound = default;
    #endregion

    private float speed;
    private float damage;
    private float MAX_PROJECTILE_LIFETIME = 10f;


    #region Unity Methods
    // NOTE: Need to use start in case of collision with another object that may not be initialized in "Awake"
    void Start()
    {
        // Check for initial collisions
        Collider[] initialCollisions = Physics.OverlapSphere(transform.position, 0.1f, collisionMask);
        if (initialCollisions.Length > 0)
        {
            OnCollision(initialCollisions[0].gameObject);
        }

        Wait(MAX_PROJECTILE_LIFETIME, () =>
        {
            Destroy(gameObject);
        });
    }

    void Update()
    {
        // Check collisions safely by raycasting (prevents clipping through obstacles).
        //   If a collision will happen within the frame, move to within collision distance.
        float frameMoveDistance = speed * Time.deltaTime;
        if (CheckCollisions(frameMoveDistance, out float distanceToCollision))
        {
            transform.Translate(Vector3.forward * distanceToCollision);
        }
        else
        {
            transform.Translate(Vector3.forward * frameMoveDistance);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        OnCollision(collision.gameObject);
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Initialize the projectile from the weapon
    /// </summary>
    /// <param name="speed">Projectile speed</param>
    /// <param name="damage">Projectile damage</param>
    public void Init(float speed, float damage)
    {
        this.speed = speed;
        this.damage = damage;
    }

    /// <summary>
    /// Handle collision with other entities
    /// </summary>
    /// <param name="collidingObject">Colliding game object</param>
    private void OnCollision(GameObject collidingObject)
    {
        if (!collisionMask.ContainsLayer(collidingObject.layer)) return;

        // Apply damage if appropriate (enemies, etc)
        Damageable damageable = collidingObject.GetComponent<Damageable>();
        if (damageable != null)
        {
            damageable.TakeDamage(damage, collidingObject);
        }

        // Use hit object material for the impact effects
        Enemy enemy = collidingObject.GetComponent<Enemy>();
        Material collidingMaterial = collidingObject.GetComponent<Renderer>().material;
        GameObject damageEffect = enemy != null ? enemy.Damageable.DamageEffect : hitEffect;
        if (damageEffect)
        {
            damageEffect.GetComponent<Renderer>().material = collidingMaterial;
            Instantiate(
                damageEffect,
                transform.position,
                Quaternion.identity,
                TemporaryObjectsManager.Instance.TemporaryChildren
            );
        }

        AudioClip hitAudio = enemy != null ? enemy.Damageable.DamageSound : hitSound;
        if (hitAudio)
        {
            AudioManager.Instance.PlayEffect(hitAudio, transform.position);
        }

        Destroy(gameObject);
    }

    /// <summary>
    /// Check whether a collision will occur within the frame (prevents collision clipping)
    /// </summary>
    /// <param name="frameDistance">Distance that will be moved in a frame</param>
    /// <param name="hitDistance">Distance to the hit</param>
    /// <returns></returns>
    private bool CheckCollisions(float frameDistance, out float hitDistance)
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, frameDistance, collisionMask))
        {
            hitDistance = hit.distance;
            return true;
        }

        hitDistance = 0;
        return false;
    }
    #endregion
}
