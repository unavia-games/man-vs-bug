﻿using Sirenix.OdinInspector;
using Unavia.Utilities;
using UnityEditorInternal;
using UnityEngine;

public enum WeaponState
{
    READY,
    FIRING,
    EMPTY,
    RELOADING
}

public enum WeaponFireType
{
    AUTOMATIC,
    // BURST,
    // BOLT,
    // PUMP,
    SEMIAUTOMATIC
}

public enum WeaponCategory
{
    RIFLE,
    SHOTGUN,
    PISTOL
}

public class Weapon : ExtendedMonoBehaviour
{
    #region Fields
    [Header("Properties")]
    [SerializeField]
    private new string name = "";
    [SerializeField]
    private WeaponFireType weaponFireType = WeaponFireType.SEMIAUTOMATIC;
    [SerializeField]
    private WeaponCategory weaponCategory = WeaponCategory.RIFLE;
    [SerializeField]
    [ReadOnly]
    private WeaponState weaponState = WeaponState.READY;

    [Header("Projectiles")]
    [SerializeField]
    [Range(5f, 50f)]
    private float muzzleVelocity = 20f;
    [SerializeField]
    [Range(1f, 100f)]
    private float projectileDamage = 5f;
    [SerializeField]
    [Range(1, 50)]
    private int clipSize = 10;
    [SerializeField]
    [ReadOnly]
    private int bulletsInClip;
    [SerializeField]
    [Range(0f, 20f)]
    private float spreadAngle = 1f;

    [Header("Timing")]
    [SerializeField]
    [Range(0f, 1f)]
    private float fireRate = 0.25f;
    [SerializeField]
    [Range(0.25f, 2f)]
    private float equipTime = 1f;
    [SerializeField]
    [Range(0.25f, 2f)]
    private float reloadTime = 1f;

    [Header("Game Objects")]
    [SerializeField]
    private GameObject projectilePrefab = default;
    [SerializeField]
    private Transform firingTransform = default;

    [Header("Effects")]
    [SerializeField]
    private AudioClip fireSound = default;
    [SerializeField]
    private AudioClip emptySound = default;
    [SerializeField]
    private AudioClip reloadSound = default;
    #endregion

    public float EquipTime => equipTime;
    public WeaponFireType WeaponFireType => weaponFireType;


    #region Unity Methods
    void Awake()
    {
        bulletsInClip = clipSize;
        weaponState = WeaponState.READY;
        gameObject.name = name;
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Fire a bullet
    /// </summary>
    public void Fire()
    {
        // Cannot fire with empty gun
        if (weaponState == WeaponState.EMPTY)
        {
            // TODO: Play sound effect
            return;
        }

        // Cannot fire while reloading OR immediately after firing
        if (weaponState == WeaponState.RELOADING || weaponState == WeaponState.FIRING) return;

        // Some weapons have a spread angle range while firing (typically automatics)
        float projectileSpreadAngle = Random.Range(-spreadAngle, spreadAngle);
        Quaternion projectileSpreadRotation = firingTransform.rotation * Quaternion.Euler(0f, projectileSpreadAngle, 0f);

        // TODO: Instantiate projectile from pool
        GameObject projectileObject = Instantiate(
            projectilePrefab,
            firingTransform.position,
            projectileSpreadRotation,
            TemporaryObjectsManager.Instance.TemporaryChildren
        );
        Projectile projectile = projectileObject.GetComponent<Projectile>();
        projectile?.Init(muzzleVelocity, projectileDamage);

        // TODO: Play sound effect and eject clip

        bulletsInClip--;
        weaponState = WeaponState.FIRING;

        // Limit fire rate to prevent rapid-firing (unless empty)
        if (bulletsInClip > 0)
        {
            Wait(fireRate, () =>
            {
                weaponState = WeaponState.READY;
            });
        }
        else
        {
            weaponState = WeaponState.EMPTY;
        }
    }

    /// <summary>
    /// Reload the weapon
    /// </summary>
    public void Reload()
    {
        // Can only reload while empty or not firing
        if (weaponState != WeaponState.EMPTY && weaponState != WeaponState.READY) return;

        // TODO: Play sound effect and eject old clip

        weaponState = WeaponState.RELOADING;
        Wait(reloadTime, () =>
        {
            bulletsInClip = clipSize;
            weaponState = WeaponState.READY;
        });
    }
    #endregion
}
