﻿using System;
using UnityEngine;

public class Damageable : MonoBehaviour
{
    #region Fields
    [SerializeField]
    [Range(0f, 100f)]
    private float maxHealth = 10f;

    [Header("Effects")]
    public GameObject DamageEffect = default;
    public GameObject DeathEffect = default;
    public AudioClip DamageSound = default;
	#endregion

    public float Health { get; private set; }
    public bool Alive { get; private set; }

    // NOTE: Use UnityEvents to only notify this game object
    [HideInInspector]
    public Action<float, GameObject> OnDamage;
    [HideInInspector]
    public Action<GameObject> OnDeath;


    #region Unity Methods
    void Awake()
    {
        Health = maxHealth;
        Alive = true;
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Immediately cause death
    /// </summary>
    /// <param name="killer">Object causing death</param>
    public void Kill(GameObject killer)
    {
        Die(killer);
    }

    /// <summary>
    /// Take damage from a source
    /// </summary>
    /// <param name="damage">Amount of damage</param>
    /// <param name="damager">Object inflicting damage</param>
    public void TakeDamage(float damage, GameObject damager)
    {
        if (!Alive) return;

        // Notify damageable entity that damage has been taken
        OnDamage?.Invoke(damage, damager);

        Health -= damage;
        if (Health <= 0)
        {
            Die(damager);
        }
    }

    /// <summary>
    /// Kill the damageable object
    /// </summary>
    /// <param name="killer">Object causing death</param>
    private void Die(GameObject killer)
    {
        if (!Alive) return;

        Health = 0f;
        Alive = false;

        // Notify damageable entity about death
        OnDeath?.Invoke(killer);

        Destroy(gameObject);
    }
    #endregion
}
