﻿using Drawing;
using Pathfinding;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RichAI))]
public class Wanderer : MonoBehaviour
{
    #region Fields
    [SerializeField]
    [MinMaxSlider(1f, 10f, true)]
    private Vector2 wanderPointRefreshRate = new Vector2(2f, 5f);
    [SerializeField]
    [Tooltip("Using radius for wander is faster but less natural")]
    private bool useRadius = false;
    [SerializeField]
    [Range(1f, 20f)]
    private float wanderRadius = 5f;
    #endregion

    private Coroutine wanderCoroutine;
    private bool isWandering;
    private RichAI pathFinder;
    private Vector3 randomDestination;


    #region Unity Methods
    void Awake()
    {
        pathFinder = GetComponent<RichAI>();
    }

    void Update()
    {
        if (isWandering)
        {
            Draw.Circle(randomDestination, transform.up, 0.25f, Color.green);
        }
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Start randomly wandering
    /// </summary>
    public void StartWander()
    {
        if (isWandering) return;

        isWandering = true;
        wanderCoroutine = StartCoroutine(Wander());
    }

    /// <summary>
    /// Stop randomly wandering
    /// </summary>
    public void StopWander()
    {
        if (!isWandering) return;

        isWandering = false;
        StopCoroutine(wanderCoroutine);
    }

    /// <summary>
    /// Randomly wander as long as coroutine runs
    /// </summary>
    /// <returns>Coroutine</returns>
    private IEnumerator Wander()
    {
        while (true)
        {
            if (!pathFinder.enabled) yield return null;

            // Radius searches are far faster but yield less natural results
            if (useRadius)
            {
                randomDestination = GetRandomPointInSphere(transform.position, wanderRadius);
            }
            else
            {
                NavGraph graph = AstarPath.active.data.graphs[0];
                List<GraphNode> nodes = new List<GraphNode>();
                graph.GetNodes(nodes.Add);
                GraphNode randomNode = nodes[Random.Range(0, nodes.Count)];
                randomDestination = (Vector3)randomNode.position;
            }

            pathFinder.destination = randomDestination;
            pathFinder.SearchPath();

            // Wait until the agent has reached its destination to calculate the next destination
            while (!pathFinder.reachedDestination) yield return null;

            // Wait a random amount of time before calculating the next destination
            float randomWaitTime = Random.Range(wanderPointRefreshRate.x, wanderPointRefreshRate.y);
            yield return new WaitForSeconds(randomWaitTime);
        }
    }

    /// <summary>
    /// Find a random accessible navigation point
    /// </summary>
    /// <param name="origin">Wander agent origin</param>
    /// <param name="radius">Wanter radius</param>
    /// <returns>Random navigation point</returns>
    private Vector3 GetRandomPointInSphere(Vector3 origin, float radius)
    {
        Vector3 randomPoint = Random.insideUnitSphere * radius;
        randomPoint.y = 0;
        randomPoint += transform.position;
        return randomPoint;
    }
    #endregion
}
