﻿public static class GameEventStrings
{
    public static string ENEMY__DAMAGED = "ENEMY__DAMAGED";
    public static string ENEMY__DEATH = "ENEMY__DEATH";

    public static string PLAYER__DAMAGED = "PLAYER__DAMAGED";
    public static string PLAYER__DEATH = "PLAYER__DEATH";
}
