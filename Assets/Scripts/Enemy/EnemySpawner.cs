﻿using com.ootii.Messages;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using Unavia.Utilities;
using UnityEngine;

public class EnemySpawner : ExtendedMonoBehaviour
{
    #region Fields
    [SerializeField]
    private List<Wave> waves = new List<Wave>();
    [SerializeField]
    [Range(0f, 10f)]
    private float timeBetweenWaves = 2f;
    [SerializeField]
    [Required]
    private Player player;

    [Header("Game Objects")]
    [SerializeField]
    private Transform spawnedChildren;
    #endregion

    private Wave currentWave;
    private int waveNumber;
    private Coroutine spawnCoroutine;
    private int enemiesRemainingToSpawn;
    private int enemiesRemaining;


    #region Unity Methods
    void Awake()
    {
        // Use own transform for enemy spawn parent if none was provided
        if (spawnedChildren == null)
        {
            spawnedChildren = transform;
        }

        MessageDispatcher.AddListener(GameEventStrings.PLAYER__DEATH, OnPlayerDeath);
        MessageDispatcher.AddListener(GameEventStrings.ENEMY__DEATH, OnEnemyDeath);
    }

    void Start()
    {
        NextWave();
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(GameEventStrings.PLAYER__DEATH, OnPlayerDeath);
        MessageDispatcher.RemoveListener(GameEventStrings.ENEMY__DEATH, OnEnemyDeath);
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Begin the next wave
    /// </summary>
    private void NextWave()
    {
        // TODO: Handle finishing all waves
        // TODO: Possibly display wave counter UI

        if (++waveNumber > waves.Count) return;
        if (player == null || !player.Alive) return;

        currentWave = waves[waveNumber - 1];

        enemiesRemainingToSpawn = currentWave.EnemyCount;
        enemiesRemaining = enemiesRemainingToSpawn;

        Wait(timeBetweenWaves, () =>
        {
            spawnCoroutine = StartCoroutine(SpawnEnemy());
        });
    }

    /// <summary>
    /// Spawn enemies while the round lasts
    /// </summary>
    /// <returns>Coroutine</returns>
    private IEnumerator SpawnEnemy()
    {
        while (enemiesRemainingToSpawn > 0)
        {
            if (player == null)
            {
                StopCoroutine(spawnCoroutine);
                yield return null;
            }

            enemiesRemainingToSpawn--;

            GameObject enemyObject = Instantiate(
                currentWave.EnemyPrefab,
                Vector3.zero,
                transform.position.RotationTo(player.transform.position),
                spawnedChildren
            );
            Enemy enemy = enemyObject.GetComponent<Enemy>();
            enemy.Init(player);

            yield return new WaitForSeconds(currentWave.TimeBetweenSpawns);
        }

        StopCoroutine(spawnCoroutine);
    }

    private void OnEnemyDeath(IMessage message)
    {
        if (--enemiesRemaining > 0) return;

        // Waves begin immediately after the previous one ends
        NextWave();
    }

    private void OnPlayerDeath(IMessage message)
    {
        StopCoroutine(spawnCoroutine);
    }
    #endregion
}
