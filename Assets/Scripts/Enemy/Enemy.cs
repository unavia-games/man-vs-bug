﻿using com.ootii.Messages;
using Drawing;
using Pathfinding;
using Sirenix.OdinInspector;
using System.Collections;
using Unavia.Utilities;
using UnityEngine;

public enum EnemyState
{
    ATTACKING,
    IDLE,
    PURSUING
}


[RequireComponent(typeof(Damageable))]
[RequireComponent(typeof(RichAI))]
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Seeker))]
[RequireComponent(typeof(Wanderer))]
public class Enemy : ExtendedMonoBehaviour
{
    #region Fields
    [SerializeField]
    [ReadOnly]
    private EnemyState state = EnemyState.IDLE;
    [SerializeField]
    [Range(1f, 10f)]
    private float speed = 5f;
    [SerializeField]
    [Range(0f, 1f)]
    [Tooltip("Rate navmesh path to target is refreshed")]
    private float destinationRefreshRate = 0.25f;

    [Header("Attack")]
    [SerializeField]
    [Range(0f, 100f)]
    private float attackDamage = 5f;
    [SerializeField]
    [Range(1, 10)]
    private float lungeSpeed = 2f;
    [SerializeField]
    [Range(0.25f, 2f)]
    private float attackDistanceThreshold = 0.5f;
    [SerializeField]
    [Range(0.5f, 5f)]
    private float timeBetweenAttacks = 1f;
    [SerializeField]
    private Color attackColor = Color.red;
    #endregion

    public Damageable Damageable { get; private set; }
    private bool alive => Damageable.Alive;
    private bool hasTarget => target != null;

    private Transform target = null;
    private Vector3 targetPosition;
    private float targetCollisionRadius;

    private RichAI pathFinder;
    private Wanderer wanderer;
    private Rigidbody rb;
    private Material skinMaterial;

    private Color originalSkinColor;
    private Coroutine attackCoroutine;
    private bool canAttack = true;
    private bool hasAppliedDamageOnAttack = false;
    private float collisionRadius;


    #region Unity Methods
    void Awake()
    {
        Damageable = GetComponent<Damageable>();
        rb = GetComponent<Rigidbody>();
        pathFinder = GetComponent<RichAI>();
        skinMaterial = GetComponent<Renderer>().material;
        wanderer = GetComponent<Wanderer>();

        state = EnemyState.PURSUING;
        collisionRadius = GetComponent<CapsuleCollider>().radius;
        originalSkinColor = skinMaterial.color;

        // Subscribe to own death and damage events
        Damageable.OnDeath += OnDeath;
        Damageable.OnDamage += OnDamage;

        MessageDispatcher.AddListener(GameEventStrings.PLAYER__DEATH, OnTargetDeath);
    }

    void Update()
    {
        if (!alive) return;

        if (hasTarget && canAttack)
        {
            float distanceToTarget = Vector3.Distance(transform.position, target.position);
            distanceToTarget = distanceToTarget - collisionRadius - targetCollisionRadius;

            // Lunge attack is triggered by proximity to player
            if (distanceToTarget <= attackDistanceThreshold)
            {
                attackCoroutine = StartCoroutine(Attack());

                canAttack = false;
                hasAppliedDamageOnAttack = false;
                Wait(timeBetweenAttacks, () =>
                {
                    canAttack = true;
                    hasAppliedDamageOnAttack = true;
                });
            }
        }

        // DEBUG
        Draw.ingame.ArrowheadArc(transform.position, transform.forward, 0.5f, Color.green);
        if (hasTarget && targetPosition != null)
        {
            Draw.ingame.Circle(targetPosition, transform.up, 0.25f, Color.green);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // Enemy can only do damage while attacking
        if (state != EnemyState.ATTACKING) return;

        // Apply damage to collider IF enemy hasn't applied damage yet this attack
        Damageable damageable = other.gameObject.GetComponent<Damageable>();
        if (damageable != null && !hasAppliedDamageOnAttack)
        {
            // Prevent further damage from this attack
            hasAppliedDamageOnAttack = true;

            // TODO: Possibly apply backwards force to enemy or player?

            // Apply damage to both enemy and collision target
            Damageable.TakeDamage(attackDamage, other.gameObject);
            damageable.TakeDamage(attackDamage, gameObject);
        }
    }

    private void OnDestroy()
    {
        MessageDispatcher.RemoveListener(GameEventStrings.PLAYER__DEATH, OnTargetDeath);
    }
    #endregion


    #region Custom Methods
    /// <summary>
    /// Initialization after spawning
    /// </summary>
    /// <param name="player">Player</param>
    public void Init(Player player)
    {
        // Enemy should wander if spawning after player death
        if (player == null || !player.Alive)
        {
            wanderer.StartWander();
            return;
        }

        target = player.transform;
        targetCollisionRadius = target.GetComponent<CapsuleCollider>().radius;

        // Set AI pathfinding properties
        pathFinder.maxSpeed = speed;

        // Path to target is calculated sporadically for performance
        StartCoroutine(UpdateAgentPath());
    }

    /// <summary>
    /// Attack the enemy's target
    /// </summary>
    /// <returns>Coroutine</returns>
    private IEnumerator Attack()
    {
        if (!hasTarget) yield return null;

        state = EnemyState.ATTACKING;
        pathFinder.canMove = false;
        skinMaterial.color = attackColor;

        // NOTE: Prevent enemy from strangely jittering after collision with player
        GetComponent<Collider>().isTrigger = true;
        rb.isKinematic = true;

        // Calculate direction and position of lunge (will reset after finished)
        Vector3 originalPosition = transform.position;
        Vector3 attackDirection = transform.DirectionTo(target);
        Vector3 attackPosition = target.position - attackDirection * (collisionRadius + targetCollisionRadius - 0.1f);

        float lungePercent = 0f;

        // Lunge towards target (lunge halts when colliding with player
        while (lungePercent < 1 && state == EnemyState.ATTACKING)
        {
            lungePercent += Time.deltaTime * lungeSpeed;
            float interpolation = (-Mathf.Pow(lungePercent, 2) + lungePercent) * 4;

            transform.position = Vector3.Lerp(originalPosition, attackPosition, interpolation);

            yield return null;
        }

        GetComponent<Collider>().isTrigger = false;
        rb.isKinematic = false;

        state = EnemyState.PURSUING;
        pathFinder.canMove = true;
        skinMaterial.color = originalSkinColor;
    }

    /// <summary>
    /// Update navmesh agent path infrequently (for performance)
    /// </summary>
    /// <returns></returns>
    private IEnumerator UpdateAgentPath()
    {
        while (hasTarget)
        {
            if (!alive) yield return null;
            // Only update agent path while pursuing
            if (state != EnemyState.PURSUING) yield return null;

            // Move enemy towards player and within range of attack (but not into player collider)
            Vector3 targetDirection = transform.DirectionTo(target);
            float targetOffsetDistance = attackDistanceThreshold / 4;
            targetPosition = target.position - targetDirection * (collisionRadius + targetCollisionRadius + targetOffsetDistance);

            // Set navmesh pathfinder destination and immediately find a new path
            pathFinder.destination = targetPosition;
            // NOTE: Auto-searching has been disabled in favour of this coroutine!
            pathFinder.SearchPath();

            yield return new WaitForSeconds(destinationRefreshRate);
        }
    }

    /// <summary>
    /// Respond to target death (start wandering)
    /// </summary>
    /// <param name="message">Message</param>
    private void OnTargetDeath(IMessage message)
    {
        target = null;
        state = EnemyState.IDLE;

        if (!Damageable.Alive) return;

        // Enemy should start wandering after target death
        wanderer.StartWander();
    }

    private void OnDamage(float damage, GameObject damager)
    {
        MessageDispatcher.SendMessage(GameEventStrings.ENEMY__DAMAGED);
    }

    private void OnDeath(GameObject killer)
    {
        MessageDispatcher.SendMessage(GameEventStrings.ENEMY__DEATH);

        if (Damageable.DeathEffect != null)
        {
            // TODO: Calculate death angle better
            Instantiate(
                Damageable.DeathEffect,
                transform.position,
                Quaternion.AngleAxis(90, killer.gameObject.transform.right),
                TemporaryObjectsManager.Instance.TemporaryChildren
            );
        }
    }
    #endregion
}
