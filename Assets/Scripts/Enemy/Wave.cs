﻿using System;
using UnityEngine;

[Serializable]
public class Wave
{
	#region Fields
	public GameObject EnemyPrefab;
	[Range(1, 100)]
	public int EnemyCount = 25;
	[Range(0.5f, 5f)]
	[Tooltip("Spawns per second")]
	public float TimeBetweenSpawns = 2f;
	#endregion
}
